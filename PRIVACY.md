# What information do we collect?

> We collect personal information that you provide to us. Some additional data — such as IP address and/or browser and device characteristics — is collected automatically when you visit our Services or Apps. Lastly, with your consent, we may also collect information regarding your geo-location, mobile device characteristics, push notification preferences, and other metadata when you use our Apps.

## Personal information you disclose to us

We collect personal information that you voluntarily provide to us when registering at the Services or Apps, expressing an interest in obtaining information about us or our products and services, when participating in activities on the Services or Apps (such as posting messages in our online forums or entering competitions, contests or giveaways) or otherwise contacting us.

The personal information that we collect depends on the context of your interactions with us and the Services or Apps, the choices you make and the products and features you use. The personal information we collect can include the following:

-   **Publicly Available Personal Information.** We collect first name, maiden name, last name, and nickname; phone numbers; email addresses; ID; current and former address; family member names and their related information; and other similar data.

-   **Personal Information Provided by You.** We collect app usage; political and social affiliation to different groups; past and current partners; data collected from surveys; financial information (credit card number, purchase history, invoices); and other similar data.

-   **Payment Data.** We collect data necessary to process your payment if you make purchases, such as your payment instrument number (such as a credit card number), and the security code associated with your payment instrument. All payment data is stored by Stripe Inc.. You may find their privacy policy link(s) here: [https://stripe.com/privacy](https://stripe.com/privacy).

All personal information that you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.

## Information automatically collected

We automatically collect certain information when you visit, use or navigate the Services or Apps. This information does not reveal your specific identity (like your name or contact information) but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Services or Apps and other technical information. This information is primarily needed to maintain the security and operation of our Services or Apps, and for our internal analytics and reporting purposes.

Like many businesses, we also collect information through cookies and similar technologies.

**Online Identifiers.** We collect devices; tools and protocols, such as IP (Internet Protocol) addresses; cookie identifiers, or others such as the ones used for analytics and marketing; Radio Frequency Identification (RFID) tags; device's geolocation; and other similar data.

## Information collected through our Apps

If you use our Apps, we may also collect the following information:

-   **Geo-Location Information.** We may request access or permission to and track location-based information from your mobile device, either continuously or while you are using our mobile application, to provide location-based services. If you wish to change our access or permissions, you may do so in your device's settings.

-   **Mobile Device Access.** We may request access or permission to certain features from your mobile device, including your mobile device's calendar, camera, contacts, microphone, bluetooth, and other features. If you wish to change our access or permissions, you may do so in your device's settings.

-   **Mobile Device Data.** We may automatically collect device information (such as your mobile device ID, model and manufacturer), operating system, version information and IP address.

-   **Push Notifications.** We may request to send you push notifications regarding your account or the mobile application. If you wish to opt-out from receiving these types of communications, you may turn them off in your device's settings.

# How do we use your information?

> We process your information for purposes based on legitimate business interests, the fulfillment of our contract with you, compliance with our legal obligations, and/or your consent.

We use personal information collected via our Services or Apps for a variety of business purposes described below. We process your personal information for these purposes in reliance on our legitimate business interests, in order to enter into or perform a contract with you, with your consent, and/or for compliance with our legal obligations. We indicate the specific processing grounds we rely on next to each purpose listed below.

We use the information we collect or receive:

-   **To facilitate account creation and logon process.** If you choose to link your account with us to a third party account (such as your Google or Facebook account), we use the information you allowed us to collect from those third parties to facilitate account creation and logon process for the performance of the contract.

-   **To send administrative information to you.** We may use your personal information to send you product, service and new feature information and/or information about changes to our terms, conditions, and policies.

-   **Fulfill and manage your orders.** We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the Services or Apps.

-   **To protect our Services.** We may use your information as part of our efforts to keep our Services or Apps safe and secure (for example, for fraud monitoring and prevention).

-   **To enable user-to-user communications.** We may use your information in order to enable user-to-user communications with each user's consent.

-   **To enforce our terms, conditions and policies for Business Purposes, Legal Reasons and Contractual.**

-   **To respond to legal requests and prevent harm.** If we receive a subpoena or other legal request, we may need to inspect the data we hold to determine how to respond.

-   **To manage user accounts**. We may use your information for the purposes of managing our account and keeping it in working order.
-   **To deliver services to the user.** We may use your information to provide you with the requested service.

-   **To respond to user inquiries/offer support to users.** We may use your information to respond to your inquiries and solve any potential issues you might have with the use of our Services.

-   **For other Business Purposes.** We may use your information for other Business Purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Services or Apps, products, marketing and your experience. We may use and store this information in aggregated and anonymized form so that it is not associated with individual end users and does not include personal information. We will not use identifiable personal information without your consent.

# Will your information be shared?

> We only share information with your consent, to comply with laws, to provide you with services, to protect your rights, or to fulfill business obligations.

We may process or share data based on the following legal basis:

-   **Consent:** We may process your data if you have given us specific consent to use your personal information in a specific purpose.

-   **Legitimate Interests:** We may process your data when it is reasonably necessary to achieve our legitimate business interests.

-   **Performance of a Contract:** Where we have entered into a contract with you, we may process your personal information to fulfill the terms of our contract.

-   **Legal Obligations:** We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).

-   **Vital Interests:** We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.

More specifically, we may need to process your data or share your personal information in the following situations:

-   **Vendors, Consultants and Other Third-Party Service Providers.** We may share your data with third party vendors, service providers, contractors or agents who perform services for us or on our behalf and require access to such information to do that work. Examples include: payment processing, data analysis, email delivery, hosting services, customer service and marketing efforts. We may allow selected third parties to use tracking technology on the Services or Apps , which will enable them to collect data about how you interact with the Services or Apps over time. This information may be used to, among other things, analyze and track data, determine the popularity of certain content and better understand online activity. Unless described in this Policy, we do not share, sell, rent or trade any of your information with third parties for their promotional purposes. We have contracts in place with our data processors. This means that they cannot do anything with your personal information unless we have instructed them to do it. They will not share your personal information with any organisation apart from us. They will hold it securely and retain it for the period we instruct.

-   **Business Transfers.** We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.

-   **Other Users.** When you share personal information (for example, by posting comments, contributions or other content to the Services or Apps ) or otherwise interact with public areas of the Services or Apps , such personal information may be viewed by all users and may be publicly distributed outside the Services or Apps in perpetuity. Similarly, other users will be able to view descriptions of your activity, communicate with you within our Services or Apps , and view your profile.

# Who will your information be shared with?

> We only share information in a manner that is essential to providing our Services.

We only share and disclose your information with the following third parties. We have included a general overview of the purpose of our data collection and processing practices. If we have processed your data based on your consent and you wish to revoke your consent, please contact us.

| Organization           | Purpose for Sharing                                 |
| ---------------------- | --------------------------------------------------- |
| **DigitalOcean, LLC.** | Cloud computing services; data backup and security. |
| **Stripe, Inc.**       | Payment processing; invoicing and billing.          |
| **ZEIT, Inc.**         | Website hosting; web and mobile analytics.          |

# Do we use cookies and other tracking technologies?

> We may use cookies and other tracking technologies to collect and store your information.

We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information. Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Policy .

# How long do we keep your information?

> We keep your information for as long as necessary to fulfill the purposes outlined in this privacy policy unless otherwise required by law.

We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy , unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). No purpose in this policy will require us keeping your personal information for longer than the period of time in which users have an account with us .

When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.

# How do we keep your information safe?

> We aim to protect your personal information through a system of organizational and technical security measures.

We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process. However, please also remember that we cannot guarantee that the internet itself is 100% secure. Although we will do our best to protect your personal information, transmission of personal information to and from our Services or Apps is at your own risk. You should only access the services within a secure environment.

# What are your privacy rights?

> You may review, change, or terminate your account at any time.

If you are resident in the European Economic Area and you believe we are unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority. You can find their contact details here: [http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm](http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm).

If you have questions or comments about your privacy rights, you may email us at [privacy@clique.app](mailto:privacy@clique.app) .

# Account Information

If you would at any time like to review or change the information in your account or terminate your account, you can:

-   Log into your account settings and update your user account.

-   Contact us using the contact information provided.

Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use and/or comply with legal requirements.

**Cookies and similar technologies:** Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Services or Apps . To opt-out of interest-based advertising by advertisers on our Services or Apps visit [http://www.aboutads.info/choices/](http://www.aboutads.info/choices/) .

**Opting out of email marketing:** You can unsubscribe from our marketing email list at any time by clicking on the unsubscribe link in the emails that we send or by contacting us using the details provided below. You will then be removed from the marketing email list – however, we will still need to send you service-related emails that are necessary for the administration and use of your account. To otherwise opt-out, you may:

-   Update your preferences through the Settings page.

-   Contact us through the Support Center.

# Data Breach Policy

A privacy breach occurs when there is unauthorized access to or collection, use, disclosure or disposal of personal information. You will be notified about data breaches when Vita LLC believes you are likely to be at risk or serious harm. For example, a data breach may be likely to result in serious financial harm or harm to your mental or physical well-being.

In the event that Vita LLC becomes aware of a security breach which has resulted or may result in unauthorized access, use or disclosure of personal information Vita LLC will promptly investigate the matter and notify the applicable Supervisory Authority not later than 72 hours after having become aware of it, unless the personal data breach is unlikely to result in a risk to the rights and freedoms of natural persons.

# Controls for Do-Not-Track Features

Most web browsers and some mobile operating systems and mobile applications include a Do-Not-Track (“DNT”) feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. No uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online.

If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this privacy policy.

# Do California residents have specific privacy rights?

> Residents of California are granted specific rights regarding access to their personal information. The rights granted to those individuals are also granted to _all_ individuals who use our Services, regardless of state or country of residence.

California Civil Code Section 1798.83, also known as the “Shine The Light” law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.

If you are under 18 years of age, reside in California, and have a registered account with the Services or Apps, you have the right to request removal of unwanted data that you publicly post on the Services or Apps. To request removal of such data, please contact us using the contact information provided below, and include the email address associated with your account and a statement that you reside in California. We will make sure the data is not publicly displayed on the Services or Apps, but please be aware that the data may not be completely or comprehensively removed from our systems.

# Do we make updates to this policy?

> Yes, we will update this policy as necessary to stay compliant with relevant laws.

We may update this privacy policy from time to time. The updated version will be indicated by an updated “Revised” date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy policy , we may notify you either by prominently posting a notice of such changes or by directly sending you a notification. We encourage you to review this privacy policy frequently to be informed of how we are protecting your information.

# How can you review, update, or delete the data we collect from you?

Based on the laws of some countries, you may have the right to request access to the personal information we collect from you, change that information, or delete it in some circumstances. To request to review, update, or delete your personal information, please visit: [https://clique.app/support](https://clique.app/support) . We will respond to your request within 30 days.

# How can you contact us about this policy?

If you have questions or comments about this policy, please contact our privacy team using one the methods below:

-   **Message our Support team directly through the Clique website or Apps.** We recommend this method for the fastest possible response.

-   **Email our Privacy team directly at [privacy@clique.app](mailto:privacy@clique.app).** Inquiries sent via email will generally be addressed in 1–2 business days.

-   **Send a request via post to the address below.** Be aware that responses to requests sent this way will take additional time, potentially up to 14 business days.
